FROM node:18.9.0 as build

COPY . /app

WORKDIR /app

RUN npm install; \
    npm run build

FROM nginx:latest as nginx

COPY --from=build /app/build /usr/share/nginx/html
COPY --from=build /app/conf/nginx.conf /etc/nginx/conf.d/react.conf
