import { ApiResource } from "../utils/types";

export interface Book extends ApiResource {
  isbn?: string;
  title?: string;
  description?: string;
  author?: string;
  publicationDate?: string;
  reviews?: any;
}
