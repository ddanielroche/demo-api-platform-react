import { ApiResource } from "../utils/types";

export interface Review extends ApiResource {
  body?: string;
  rating?: number;
  book?: any;
  author?: string;
  publicationDate?: string;
}
