import {routesDetail} from "../../routes/book";

export const NavBar = () => {
    return <nav style={{display: "flex", gap: 32}}> {
        routesDetail.map(rt => <a href={rt.path}>{rt.key}</a>)
    } </nav>
};
