import { useEffect } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import Field from "../Field";
import { normalizeLinks } from "../../utils/dataAccess";
import TResource from "./type";
import { SubmissionError, TError } from "../../utils/types";

interface FormProps {
  onSubmit: (item: Partial<TResource>) => any;
  initialValues?: Partial<TResource>;
  error?: TError;
  reset: () => void;
}

const Form = ({ onSubmit, error, reset, initialValues }: FormProps) => {
  const {
    register,
    setError,
    handleSubmit,
    formState: { errors },
  } = useForm<TResource>({
    defaultValues: initialValues
      ? {
          ...initialValues,
          reviews:
            initialValues["reviews"]?.map((emb: any) => emb["@id"]) ?? [],
        }
      : undefined,
  });

  useEffect(() => {
    if (error instanceof SubmissionError) {
      Object.keys(error.errors).forEach((errorPath) => {
        if (errors[errorPath as keyof TResource]) {
          return;
        }
        setError(errorPath as keyof TResource, {
          type: "server",
          message: error.errors[errorPath],
        });
      });

      reset();
    }
  }, [error, errors, reset, setError]);

  const onFormSubmit: SubmitHandler<TResource> = (data) => {
    onSubmit({
      ...data,
      reviews: normalizeLinks(data["reviews"]),
    });
  };

  return (
    <form onSubmit={handleSubmit(onFormSubmit)}>
      <Field
        register={register}
        name="isbn"
        placeholder=""
        type="text"
        errors={errors}
      />
      <Field
        register={register}
        name="title"
        placeholder=""
        type="text"
        required
        errors={errors}
      />
      <Field
        register={register}
        name="description"
        placeholder=""
        type="text"
        required
        errors={errors}
      />
      <Field
        register={register}
        name="author"
        placeholder=""
        type="text"
        required
        errors={errors}
      />
      <Field
        register={register}
        name="publicationDate"
        placeholder=""
        type="dateTime"
        required
        errors={errors}
      />
      <Field
        register={register}
        name="reviews"
        placeholder=""
        type="text"
        required
        errors={errors}
      />

      <button type="submit" className="btn btn-success">
        Submit
      </button>
    </form>
  );
};

export default Form;
