import { Route } from "react-router-dom";
import { List, Create, Update, Show } from "../components/book/";

export const routesDetail = [
  {
    path:"/books/create",
    element: <Create />,
    key: "create"
  },
  {
    path:"/books/edit/:id",
    element: <Update />,
    key: "update"
  },
  {
    path:"/books/show/:id",
    element: <Show />,
    key: "show"
  },
  {
    path:"/books",
    element: <List />,
    key: "list"
  },
  {
    path:"/books/:page",
    element: <List />,
    key: "page"
  },
  {
    path:"/",
    element: <List />,
    key: "home"
  }
];

const routes = routesDetail.map(
    rt => <Route path={rt.path} element={rt.element} key={rt.key} />
);

export default routes;
